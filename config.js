Object.assign(process.env, {
    GIT_AUTHOR_NAME: 'Renovate Bot',
    GIT_AUTHOR_EMAIL: 'renovate@localhost',
    GIT_COMMITTER_NAME: 'Renovate Bot',
    GIT_COMMITTER_EMAIL: 'renovate@localhost',
});

module.exports = {
    username: 'renovate',
    gitAuthor: 'Renovate Bot <renovate@localhost>',
    dependencyDashboardAutoClose: true,
    onboarding: true,
    ignorePrAuthor: true,
    repositories: [
        'joshbeard/renovate-runner'
    ],
};
